
// Basic recursive fibonacci
// Not efficient, just kept here as a mental reference
export function fibonacci(n: number): number {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}

// Returns the index of a found fibonacci sequence (or -1)
// Checks for the definition of a fibonacci sequence, not for "the" sequence
export function indexOfFibonacci(subject: number[], minlength = 5): number {
  let index  = 2;
  let length = 0;
  for(let index = 2; index < subject.length; index++) {

    // Check if the current index is the sum of previous 2 numbers
    if (
      (subject[index] && subject[index - 1] && subject[index - 2]) && // All truthy
      ((subject[index - 1] + subject[index - 2]) == subject[index])   // And matching fibonacci definition
    ) {

      // Mark previous 2 as part of the sequence upon init
      if (!length) {
        length = 2;
      }

      // Mark current index as part of the sequence
      length++;

      // If we match the target length, return the index which it starts on
      // -1, as we're mixing indexes and length
      if (length >= minlength) {
        return index - (length - 1);
      }
    } else {
      // Not matching
      length = 0;
    }
  }

  // No return was made in the loop, so no sequence found
  return -1;
}

// console.log([
//   [      1,1,2,3,5,8,13], // 0
//   [0,0,0,2,1,2,3,5,8,13], // 4
//   [0,0,0,2,1,2,3,5],      // -1
// ].map(seq => indexOfFibonacci(seq, 5)));
