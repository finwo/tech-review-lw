import { fibonacci, indexOfFibonacci } from './fibonacci';

type GridEntry = {
  x: number;
  y: number;
  v: number;
}

type GridSettings = {
  background: string;
  font      : string;
  color     : string;
  grid      : string;
  flash     : string;
  flashTime : number;
  match     : string;
  matchSize : number;
  size      : number;
  fieldSize : number;
  offsetX   : number;
  offsetY   : number;
}

// The grid storage itself
export class Grid {
  canvas  : HTMLCanvasElement;
  entries : GridEntry[];
  settings: GridSettings;

  constructor(public canvas, settings: GridSettings = {}) {
    this.entries = [];

    // Merge given with default settings
    const fieldSize = Math.min(canvas.width, canvas.height) / (settings.size || 50);
    this.settings = Object.assign({
      background: '#000000',
      font      : `${fieldSize}px monospace`,
      color     : '#E0E0E0',
      grid      : '#808080',
      flash     : '#FFAA00',
      flashTime : 50,
      match     : '#00DD00',
      matchSize : 5,
      size      : 50,
      fieldSize : fieldSize,
      offsetX   : 0,
      offsetY   : 0,
    }, settings);

    setTimeout(() => this.redraw(), 100);
  }

  getFieldEntry(x: number, y: number): GridEntry {
    let entry = this.entries.find(e => e.x == x && e.y == y);
    if (!entry) {
      this.entries.push(entry = { x, y, v: 0 });
    }
    return entry;
  }

  deleteFieldEntry(x: number, y: number): void {
    this.entries = this.entries.filter(entry => !(entry.x == x && entry.y == y));
  }

  getRowValues(rowIndex: number): number[] {
    const values = new Array(this.settings.size).fill(0);
    for(const entry of this.entries.filter(e => e.y == rowIndex)) {
      values[entry.x] = entry.v;
    }
    return values;
  }

  getColumnValues(columnIndex: number): number[] {
    const values = new Array(this.settings.size).fill(0);
    for(const entry of this.entries.filter(e => e.x == columnIndex)) {
      values[entry.y] = entry.v;
    }
    return values;
  }

  incrementField(x: number, y: number, redraw = true): void {
    let entry = this.getFieldEntry(x, y);
    entry.v++;
    if (redraw) this.redraw();
  }

  drawBackground(): void {
    const ctx  = this.canvas.getContext('2d');
    const size = this.settings.size * this.settings.fieldSize;
    ctx.fillStyle = this.settings.background;
    ctx.fillRect(0, 0, size, size);
  }

  drawValues(): void {
    const ctx  = this.canvas.getContext('2d');
    ctx.font      = this.settings.font;
    ctx.fillStyle = this.settings.color;
    for(const entry of this.entries) {
      if (!entry.v) continue;
      ctx.fillText(
        entry.v,
        entry.x * this.settings.fieldSize,
        (entry.y + 1) * this.settings.fieldSize, // +1, as text align = bottom-left and pixel align = top-left
        this.settings.fieldSize
      );
    }

  }

  drawGrid(): void {
    const ctx  = this.canvas.getContext('2d');
    // Vertical lines
    for(let x=0; x < this.settings.size; x += 1) {
      ctx.strokeStyle = this.settings.grid;
      ctx.beginPath();
      ctx.moveTo(x * this.settings.fieldSize, 0);
      ctx.lineTo(x * this.settings.fieldSize, this.settings.size * this.settings.fieldSize);
      ctx.stroke();
    }
    // Horizontal lines
    for(let y=0; y < this.settings.size; y += 1) {
      ctx.strokeStyle = this.settings.grid;
      ctx.beginPath();
      ctx.moveTo(0                                           , y * this.settings.fieldSize);
      ctx.lineTo(this.settings.size * this.settings.fieldSize, y * this.settings.fieldSize);
      ctx.stroke();
    }
  }

  // Flashes row+column
  async flashCross(columnIndex: number, rowIndex: number): Promise<void> {
    const ctx     = this.canvas.getContext('2d');
    const mult    = this.settings.fieldSize;
    ctx.fillStyle = this.settings.flash;
    ctx.fillRect(columnIndex * mult, 0              , mult                     , this.settings.size * mult); // Column
    ctx.fillRect(0                 , rowIndex * mult, this.settings.size * mult, mult                     ); // Row
    await new Promise(r => setTimeout(r, this.settings.flashTime));
    this.redraw();
  }

  // Flashes fibonacci-sequence matches
  async flashMatch() {
    const ctx     = this.canvas.getContext('2d');
    const mult    = this.settings.fieldSize;
    ctx.fillStyle = this.settings.match;

    // Handle column matching
    for(let x = 0; x < this.settings.size; x++) {

      // Top to bottom
      const ttbIndex = indexOfFibonacci(this.getColumnValues(x), this.settings.matchSize);
      if (~ttbIndex) {
        ctx.fillRect(x * mult, ttbIndex * mult, mult, mult * this.settings.matchSize);
        for(let y = 0; y < this.settings.matchSize; y++) this.deleteFieldEntry(x, ttbIndex + y);
      }

      // Bottom to top
      let bttIndex = indexOfFibonacci(this.getColumnValues(x).reverse(), this.settings.matchSize);
      if (~bttIndex) {
        bttIndex = this.settings.size - this.settings.matchSize - bttIndex; // Reverse index
        ctx.fillRect(x * mult, bttIndex * mult, mult, mult * this.settings.matchSize);
        for(let y = 0; y < this.settings.matchSize; y++) this.deleteFieldEntry(x, bttIndex + y);
      }

    }

    // Handle row matching
    for(let y = 0; y < this.settings.size; y++) {

      // Left to right
      const ltrIndex = indexOfFibonacci(this.getRowValues(y), this.settings.matchSize);
      if (~ltrIndex) {
        ctx.fillRect(ltrIndex * mult, y * mult, mult * this.settings.matchSize, mult);
        for(let x = 0; x < this.settings.matchSize; x++) this.deleteFieldEntry(ltrIndex + x, y);
      }

      // Right to left
      let rtlIndex = indexOfFibonacci(this.getRowValues(y).reverse(), this.settings.matchSize);
      if (~rtlIndex) {
        rtlIndex = this.settings.size - this.settings.matchSize - rtlIndex; // Reverse index
        ctx.fillRect(rtlIndex * mult, y * mult, mult * this.settings.matchSize, mult);
        for(let x = 0; x < this.settings.matchSize; x++) this.deleteFieldEntry(rtlIndex + x, y);
      }

    }

    // And reset after flashtime
    await new Promise(r => setTimeout(r, this.settings.flashTime));
    this.redraw();
  }

  async redraw(): Promise<void> {
    this.drawBackground();
    this.drawValues();
    this.drawGrid();
  }
}
