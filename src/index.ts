import { Grid } from './grid';

// Settings
const gridFields = 50;
const gridSize   = 16;

// Setup cvs sizing & location
cvs.width            = cvs.height          = (gridFields  * gridSize);
cvs.style.marginLeft = cvs.style.marginTop = `-${gridFields * gridSize / 2}px`;

// Initialize the grid
const grid = new Grid(
  document.getElementById('cvs'),
  {
    background: '#282828',
    color     : '#DDDDD0',
    grid      : '#808078',
    flash     : '#AA8822',
    flashTime : 100,
    match     : '#008800',
    matchSize : 5,
  }
);

// Handle clicks
cvs.onclick = async ev => {
  ev.preventDefault();
  const rect   = ev.target.getBoundingClientRect();
  const mouseX = ev.clientX - rect.left;
  const mouseY = ev.clientY - rect.top;
  const gridX  = Math.floor(mouseX / gridSize);
  const gridY  = Math.floor(mouseY / gridSize);

  // No redraws to save performance
  for(let x = 0; x <= gridFields ; x++) {
    grid.incrementField(x    , gridY, false);
  }
  for(let y = 0; y <= gridFields ; y++) {
    if (y == gridY) continue;
    grid.incrementField(gridX, y, false);
  }

  await grid.flashCross(gridX, gridY);
  await new Promise(r => setTimeout(r, grid.settings.flashTime))
  await grid.flashMatch();
};
