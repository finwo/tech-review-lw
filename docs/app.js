(() => {
  // src/fibonacci.ts
  function indexOfFibonacci(subject, minlength = 5) {
    let index = 2;
    let length = 0;
    for (let index2 = 2; index2 < subject.length; index2++) {
      if (subject[index2] && subject[index2 - 1] && subject[index2 - 2] && subject[index2 - 1] + subject[index2 - 2] == subject[index2]) {
        if (!length) {
          length = 2;
        }
        length++;
        if (length >= minlength) {
          return index2 - (length - 1);
        }
      } else {
        length = 0;
      }
    }
    return -1;
  }

  // src/grid.ts
  var Grid = class {
    constructor(canvas, settings = {}) {
      this.canvas = canvas;
      this.entries = [];
      const fieldSize = Math.min(canvas.width, canvas.height) / (settings.size || 50);
      this.settings = Object.assign({
        background: "#000000",
        font: `${fieldSize}px monospace`,
        color: "#E0E0E0",
        grid: "#808080",
        flash: "#FFAA00",
        flashTime: 50,
        match: "#00DD00",
        matchSize: 5,
        size: 50,
        fieldSize,
        offsetX: 0,
        offsetY: 0
      }, settings);
      setTimeout(() => this.redraw(), 100);
    }
    getFieldEntry(x, y) {
      let entry = this.entries.find((e) => e.x == x && e.y == y);
      if (!entry) {
        this.entries.push(entry = { x, y, v: 0 });
      }
      return entry;
    }
    deleteFieldEntry(x, y) {
      this.entries = this.entries.filter((entry) => !(entry.x == x && entry.y == y));
    }
    getRowValues(rowIndex) {
      const values = new Array(this.settings.size).fill(0);
      for (const entry of this.entries.filter((e) => e.y == rowIndex)) {
        values[entry.x] = entry.v;
      }
      return values;
    }
    getColumnValues(columnIndex) {
      const values = new Array(this.settings.size).fill(0);
      for (const entry of this.entries.filter((e) => e.x == columnIndex)) {
        values[entry.y] = entry.v;
      }
      return values;
    }
    incrementField(x, y, redraw = true) {
      let entry = this.getFieldEntry(x, y);
      entry.v++;
      if (redraw)
        this.redraw();
    }
    drawBackground() {
      const ctx = this.canvas.getContext("2d");
      const size = this.settings.size * this.settings.fieldSize;
      ctx.fillStyle = this.settings.background;
      ctx.fillRect(0, 0, size, size);
    }
    drawValues() {
      const ctx = this.canvas.getContext("2d");
      ctx.font = this.settings.font;
      ctx.fillStyle = this.settings.color;
      for (const entry of this.entries) {
        if (!entry.v)
          continue;
        ctx.fillText(entry.v, entry.x * this.settings.fieldSize, (entry.y + 1) * this.settings.fieldSize, this.settings.fieldSize);
      }
    }
    drawGrid() {
      const ctx = this.canvas.getContext("2d");
      for (let x = 0; x < this.settings.size; x += 1) {
        ctx.strokeStyle = this.settings.grid;
        ctx.beginPath();
        ctx.moveTo(x * this.settings.fieldSize, 0);
        ctx.lineTo(x * this.settings.fieldSize, this.settings.size * this.settings.fieldSize);
        ctx.stroke();
      }
      for (let y = 0; y < this.settings.size; y += 1) {
        ctx.strokeStyle = this.settings.grid;
        ctx.beginPath();
        ctx.moveTo(0, y * this.settings.fieldSize);
        ctx.lineTo(this.settings.size * this.settings.fieldSize, y * this.settings.fieldSize);
        ctx.stroke();
      }
    }
    async flashCross(columnIndex, rowIndex) {
      const ctx = this.canvas.getContext("2d");
      const mult = this.settings.fieldSize;
      ctx.fillStyle = this.settings.flash;
      ctx.fillRect(columnIndex * mult, 0, mult, this.settings.size * mult);
      ctx.fillRect(0, rowIndex * mult, this.settings.size * mult, mult);
      await new Promise((r) => setTimeout(r, this.settings.flashTime));
      this.redraw();
    }
    async flashMatch() {
      const ctx = this.canvas.getContext("2d");
      const mult = this.settings.fieldSize;
      ctx.fillStyle = this.settings.match;
      for (let x = 0; x < this.settings.size; x++) {
        const ttbIndex = indexOfFibonacci(this.getColumnValues(x), this.settings.matchSize);
        if (~ttbIndex) {
          ctx.fillRect(x * mult, ttbIndex * mult, mult, mult * this.settings.matchSize);
          for (let y = 0; y < this.settings.matchSize; y++)
            this.deleteFieldEntry(x, ttbIndex + y);
        }
        let bttIndex = indexOfFibonacci(this.getColumnValues(x).reverse(), this.settings.matchSize);
        if (~bttIndex) {
          bttIndex = this.settings.size - this.settings.matchSize - bttIndex;
          ctx.fillRect(x * mult, bttIndex * mult, mult, mult * this.settings.matchSize);
          for (let y = 0; y < this.settings.matchSize; y++)
            this.deleteFieldEntry(x, bttIndex + y);
        }
      }
      for (let y = 0; y < this.settings.size; y++) {
        const ltrIndex = indexOfFibonacci(this.getRowValues(y), this.settings.matchSize);
        if (~ltrIndex) {
          ctx.fillRect(ltrIndex * mult, y * mult, mult * this.settings.matchSize, mult);
          for (let x = 0; x < this.settings.matchSize; x++)
            this.deleteFieldEntry(ltrIndex + x, y);
        }
        let rtlIndex = indexOfFibonacci(this.getRowValues(y).reverse(), this.settings.matchSize);
        if (~rtlIndex) {
          rtlIndex = this.settings.size - this.settings.matchSize - rtlIndex;
          ctx.fillRect(rtlIndex * mult, y * mult, mult * this.settings.matchSize, mult);
          for (let x = 0; x < this.settings.matchSize; x++)
            this.deleteFieldEntry(rtlIndex + x, y);
        }
      }
      await new Promise((r) => setTimeout(r, this.settings.flashTime));
      this.redraw();
    }
    async redraw() {
      this.drawBackground();
      this.drawValues();
      this.drawGrid();
    }
  };

  // src/index.ts
  var gridFields = 50;
  var gridSize = 16;
  cvs.width = cvs.height = gridFields * gridSize;
  cvs.style.marginLeft = cvs.style.marginTop = `-${gridFields * gridSize / 2}px`;
  var grid = new Grid(document.getElementById("cvs"), {
    background: "#282828",
    color: "#DDDDD0",
    grid: "#808078",
    flash: "#AA8822",
    flashTime: 100,
    match: "#008800",
    matchSize: 5
  });
  cvs.onclick = async (ev) => {
    ev.preventDefault();
    const rect = ev.target.getBoundingClientRect();
    const mouseX = ev.clientX - rect.left;
    const mouseY = ev.clientY - rect.top;
    const gridX = Math.floor(mouseX / gridSize);
    const gridY = Math.floor(mouseY / gridSize);
    for (let x = 0; x <= gridFields; x++) {
      grid.incrementField(x, gridY, false);
    }
    for (let y = 0; y <= gridFields; y++) {
      if (y == gridY)
        continue;
      grid.incrementField(gridX, y, false);
    }
    await grid.flashCross(gridX, gridY);
    await new Promise((r) => setTimeout(r, grid.settings.flashTime));
    await grid.flashMatch();
  };
})();
//# sourceMappingURL=app.js.map
